package dk.s4.hl7.cda.convert.encode.qrd;

import org.apache.log4j.BasicConfigurator;

import dk.s4.hl7.cda.model.qrd.QRDDocument;

/**
 * Test QRD with no response
 *
 * @author Frank Jacobsen, Systematic
 * 
 */
public final class SetupQRDNoResponseExample extends QRDExampleBase {
  private static final String DOCUMENT_ID = "951d558e-8775-4bb5-8db4-bfa4133ea605";

  @Override
  public QRDDocument createDocument() throws Exception {
    BasicConfigurator.configure();
    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);
    return cda;
  }
}

package dk.s4.hl7.cda.convert.decode.qfdd.precondition;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition.QFDDPreconditionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDPreconditionGroup;
import dk.s4.hl7.cda.model.qfdd.QFDDPreconditionGroup.GroupType;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class PreconditionHandler extends BaseXmlHandler {
  private static final String[] GROUP_NAMES = createGroupNames();
  private CriterionHandler criterionHandler;
  private GroupHandler groupHandler;
  private final String baseXPath;
  private List<QFDDPrecondition> preconditions;
  private QFDDPreconditionGroup group;
  private boolean negationInd;
  private CodedValue conjunctionCode;

  public PreconditionHandler(String preconditionBaseXpath) {
    this.baseXPath = preconditionBaseXpath;
    addPath(baseXPath);
    addPath(baseXPath + "/conjunctionCode");
    addPath(baseXPath + "/negationInd");
    addGroups();
    this.criterionHandler = new CriterionHandler(baseXPath);
    this.groupHandler = null;
    this.preconditions = new ArrayList<QFDDPrecondition>();
    this.group = null;
    this.negationInd = false;
    this.conjunctionCode = null;
  }

  private synchronized static String[] createGroupNames() {
    GroupType[] groupTypes = GroupType.values();
    String[] groupNames = new String[groupTypes.length];
    for (int i = 0; i < groupTypes.length; i++) {
      groupNames[i] = groupTypes[i].toXmlElementName();
    }
    return groupNames;
  }

  private void addGroups() {
    for (int i = 0; i < GROUP_NAMES.length; i++) {
      addPath(baseXPath + "/" + GROUP_NAMES[i]);
      addPath(baseXPath + "/" + GROUP_NAMES[i] + "/id");
    }
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "precondition")) {
      return;
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "conjunctionCode", "code")) {
      this.conjunctionCode = new CodedValue(xmlElement.getAttributeValue("code"),
          xmlElement.getAttributeValue("codeSystem"), xmlElement.getAttributeValue("displayName"),
          xmlElement.getAttributeValue("codeSystemName"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "negationInd", "value")) {
      negationInd = getBooleanOrFalse(xmlElement);
    } else {
      for (int i = 0; i < GROUP_NAMES.length; i++) {
        if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), GROUP_NAMES[i])) {
          this.groupHandler = new GroupHandler(baseXPath, GroupType.values()[i]);
          this.groupHandler.addHandlerToMap(xmlMapping);
          break;
        }
      }
    }
  }

  private static boolean getBooleanOrFalse(XMLElement xmlElement) {
    try {
      return Boolean.parseBoolean(xmlElement.getAttributeValue("value"));
    } catch (Exception ex) {
      return false;
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "precondition")) {
      QFDDPreconditionBuilder builder = null;
      if (group != null) {
        builder = new QFDDPreconditionBuilder(group);
        group = null;
      } else if (criterionHandler.getCriterion() != null) {
        builder = new QFDDPreconditionBuilder(criterionHandler.getCriterion());
        criterionHandler.clear();
      }
      if (builder != null) {
        preconditions.add(builder.setConjunctionCode(conjunctionCode).setNegationInd(negationInd).build());
        conjunctionCode = null;
        negationInd = false;
      }
    } else if (groupHandler != null && ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(),
        groupHandler.getGroupType().toXmlElementName())) {
      group = groupHandler.getPreconditionGroup();
      groupHandler.removeHandlerFromMap(xmlMapping);
      groupHandler = null;
    }
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
    criterionHandler.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);
    criterionHandler.removeHandlerFromMap(xmlMapping);
    if (groupHandler != null) {
      groupHandler.removeHandlerFromMap(xmlMapping);
    }
  }

  public List<QFDDPrecondition> getPreconditions() {
    return new ArrayList<QFDDPrecondition>(preconditions);
  }

  public void clear() {
    preconditions.clear();
  }
}

package dk.s4.hl7.cda.convert.decode.qfdd.precondition;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDCriterion;
import dk.s4.hl7.cda.model.qfdd.QFDDCriterion.QFDDCriterionBuilder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class CriterionHandler extends BaseXmlHandler {
  private static final String BASE_ELEMENT_NAME = "criterion";
  private CodedValue code;
  private String minimum;
  private String maximum;
  private String valueType;
  private CodedValue answer;
  private QFDDCriterion criterion;

  public CriterionHandler(String criterionBase) {
    this.criterion = null;
    addPath(criterionBase + "/" + BASE_ELEMENT_NAME);
    addPath(criterionBase + "/" + BASE_ELEMENT_NAME + "/code");
    addPath(criterionBase + "/" + BASE_ELEMENT_NAME + "/value");
    addPath(criterionBase + "/" + BASE_ELEMENT_NAME + "/value/low");
    addPath(criterionBase + "/" + BASE_ELEMENT_NAME + "/value/high");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code", "codeSystem")) {
      code = new CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .build();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "type")) {
      valueType = xmlElement.getAttributeValue("type");
      if ("CE".equalsIgnoreCase(valueType)) {
        answer = new CodedValueBuilder()
            .setCode(xmlElement.getAttributeValue("code"))
            .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
            .setDisplayName(xmlElement.getAttributeValue("displayName"))
            .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
            .build();
      }
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "low", "value")) {
      minimum = xmlElement.getAttributeValue("value");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "high", "value")) {
      maximum = xmlElement.getAttributeValue("value");
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "criterion")) {
      if (answer == null) {
        criterion = new QFDDCriterionBuilder(code)
            .setMinimum(minimum)
            .setMaximum(maximum)
            .setValueType(valueType)
            .build();
      } else {
        criterion = new QFDDCriterionBuilder(code).setAnswer(answer).setValueType(valueType).build();
      }
      localClear();
    }
  }

  private void localClear() {
    code = null;
    minimum = null;
    maximum = null;
    valueType = null;
    answer = null;
  }

  public void clear() {
    localClear();
    criterion = null;
  }

  public QFDDCriterion getCriterion() {
    return criterion;
  }
}

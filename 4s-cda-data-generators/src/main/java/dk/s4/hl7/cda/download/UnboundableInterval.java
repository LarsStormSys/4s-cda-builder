/**
 * The MIT License
 *
 * Original work sponsored and donated by National Board of e-Health (SDS), Denmark
 * (http://sundhedsdatastyrelsen.dk)
 *
 * Copyright (C) 2016 National Board of e-Health (SDS), Denmark (http://sundhedsdatastyrelsen.dk)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package dk.s4.hl7.cda.download;

import org.joda.time.DateTime;
import org.joda.time.Interval;

/**
 * Interval that can be unbounded at either start or end. Intended for use
 * in queries, not for determining overlap. 
 */
public class UnboundableInterval {
  private DateTime start;
  private DateTime end;

  public UnboundableInterval(DateTime start, DateTime end) {
    super();
    this.start = start;
    this.end = end;
    validate();
  }

  public UnboundableInterval(Interval interval) {
    this(interval.getStart(), interval.getEnd());
  }

  public DateTime getStart() {
    return start;
  }

  public DateTime getEnd() {
    return end;
  }

  /**
   * Gets this interval as joda type interval, provided start and end are bounded. 
   * @return joda {@link Interval} when start and end are defined, null otherwise
   */
  public Interval getAsInterval() {
    return start != null && end != null ? new Interval(start, end) : null;
  }

  private void validate() {
    if (this.start == null && this.end == null) {
      throw new IllegalArgumentException("Start and end in interval cannot both be null");
    } else if (this.start != null && this.end != null && this.start.isAfter(this.end)) {
      throw new IllegalArgumentException("Start cannot be after end in interval");
    }
  }

  @Override
  public String toString() {
    return "UnboundableInterval [start=" + start + ", end=" + end + "]";
  }

}

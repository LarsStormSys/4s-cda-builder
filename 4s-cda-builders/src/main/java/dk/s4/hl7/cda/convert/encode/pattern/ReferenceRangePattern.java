package dk.s4.hl7.cda.convert.encode.pattern;

import java.io.IOException;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.convert.encode.BuildUtil;
import dk.s4.hl7.cda.model.qfdd.QFDDNumericQuestion;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

public class ReferenceRangePattern {
  public void build(QRDNumericResponse qrdNumericResponse, XmlStreamBuilder xmlBuilder) throws IOException {
    if (qrdNumericResponse != null && qrdNumericResponse.getIntervalType() != null
        && (qrdNumericResponse.getMinimum() != null || qrdNumericResponse.getMaximum() != null)) {
      build(qrdNumericResponse.getMinimum(), qrdNumericResponse.getMaximum(),
          qrdNumericResponse.getIntervalTypeAsString(), xmlBuilder);
    }
  }

  public void build(QFDDNumericQuestion qrdNumericResponse, XmlStreamBuilder xmlBuilder) throws IOException {
    if (qrdNumericResponse != null && qrdNumericResponse.getIntervalType() != null
        && (qrdNumericResponse.getMinimum() != null || qrdNumericResponse.getMaximum() != null)) {
      build(qrdNumericResponse.getMinimum(), qrdNumericResponse.getMaximum(),
          qrdNumericResponse.getIntervalTypeAsString(), xmlBuilder);
    }
  }

  private void build(String minimum, String maximum, String intervalType, XmlStreamBuilder xmlBuilder)
      throws IOException {
    xmlBuilder.element("referenceRange").attribute("typeCode", "REFV");
    BuildUtil.buildTemplateIds(xmlBuilder, HL7.RESPONSE_REFERENCE_RANGE_PATTERN_OID);
    xmlBuilder.element("observationRange");

    xmlBuilder.element("value").attribute("xsi:type", intervalType);
    if (minimum != null) {
      xmlBuilder.element("low").attribute("value", minimum).elementShortEnd();
    }
    if (maximum != null) {
      xmlBuilder.element("high").attribute("value", maximum).elementShortEnd();
    }
    xmlBuilder.elementEnd(); // end value
    xmlBuilder.elementEnd(); // end observationRange
    xmlBuilder.elementEnd(); // end referenceRange
  }
}

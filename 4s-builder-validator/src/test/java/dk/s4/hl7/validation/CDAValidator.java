package dk.s4.hl7.validation;

import org.junit.Assert;
import org.junit.Assume;

import generated.CdaType;
import goimplement.it.ClientBuilder;
import goimplement.it.ValidationResponse;

public class CDAValidator {
  public static void validateCDA(String document, CdaType docType) {
    Assume.assumeTrue(System.getProperty("ignore_validation") == null);
    checkResponse(callValidationService(document, docType));
  }

  private static ValidationResponse callValidationService(String document, CdaType docType) {
    return ClientBuilder.newClient().validateCdaType(docType.name()).postXmlAsJson(document, ValidationResponse.class);
  }

  private static void checkResponse(final ValidationResponse response) {
    if (!response.getErrors().isEmpty()) {
      StringBuilder builder = new StringBuilder(1024);
      builder.append("\n----------\n");
      for (goimplement.it.Error error : response.getErrors()) {
        builder.append("ErrorCode: ").append(error.getErrorCode()).append('\n');
        builder.append("Location: ").append(error.getLocation()).append('\n');
        builder.append("Message: ").append(error.getMessage()).append('\n');
        builder.append("----------\n");
      }
      Assert.fail(builder.toString());
    }
  }
}

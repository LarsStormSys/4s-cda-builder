package dk.s4.hl7.cda.convert.decode;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.QRDXmlCodec;
import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.model.qrd.QRDDocument;

public final class TestXmlToQrd extends BaseDecodeTest implements ConcurrencyTestCase {
  private Codec<QRDDocument, String> codec;

  @Before
  public void before() {
    setCodec(new QRDXmlCodec());
  }

  public void setCodec(QRDXmlCodec codec) {
    this.codec = codec;
  }

  @Test
  public void testXmlToQRD() throws Exception {
    encodeDecodeAndCompare(codec, SetupQRDDocumentForTest.defineAsCDA());
  }

  @Override
  public void runTest() throws Exception {
    testXmlToQRD();
  }
}

package dk.s4.hl7.validation;

import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import generated.CdaType;

public abstract class BaseTest<E extends ClinicalDocument> {
  private Codec<E, String> xmlCodec;

  public BaseTest(Codec<E, String> xmlCodec) {
    this.xmlCodec = xmlCodec;
  }

  protected void validateDocument(E cda, CdaType docType) throws Exception {
    CDAValidator.validateCDA(xmlCodec.encode(cda), docType);
  }
}
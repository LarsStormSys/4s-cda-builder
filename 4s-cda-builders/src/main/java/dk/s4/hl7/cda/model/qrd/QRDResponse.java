package dk.s4.hl7.cda.model.qrd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.QuestionnaireEntity;
import dk.s4.hl7.cda.model.Reference;

/**
 * Base QRD response for all types of response types
 */
public abstract class QRDResponse extends QuestionnaireEntity {
  private List<Reference> references;

  protected QRDResponse(BaseQRDResponseBuilder<?, ?> builder) {
    super(builder);
    this.references = builder.references;
  }

  public void addReference(Reference reference) {
    if (reference != null) {
      references.add(reference);
    }
  }

  public List<Reference> getReferences() {
    return references;
  }

  public static abstract class BaseQRDResponseBuilder<R extends QRDResponse, T extends QuestionnaireEntityBuilder<R, T>>
      extends QuestionnaireEntityBuilder<R, T> {
    protected List<Reference> references;

    public BaseQRDResponseBuilder() {
      references = new ArrayList<Reference>();
    }

    public T addReference(Reference reference) {
      if (reference != null) {
        references.add(reference);
      }
      return getThis();
    }

    public abstract T getThis();

    public abstract R build();
  }
}

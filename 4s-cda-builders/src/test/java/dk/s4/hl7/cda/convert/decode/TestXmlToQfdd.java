package dk.s4.hl7.cda.convert.decode;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.QFDDXmlCodec;

public class TestXmlToQfdd extends BaseDecodeTest implements ConcurrencyTestCase {
  private QFDDXmlCodec codec;

  @Before
  public void before() {
    setCodec(new QFDDXmlCodec());
  }

  public void setCodec(QFDDXmlCodec codec) {
    this.codec = codec;
  }

  @Test
  public void testQFDDComplete() throws Exception {
    encodeDecodeAndCompare(codec, SetupQFDDDocumentForTest.defineAsCDA());
  }

  @Override
  public void runTest() throws Exception {
    testQFDDComplete();
  }
}
